const { 
  task, 
  parallel, 
  src,
  dest,
} = require('gulp')
const browserify = require('browserify')
const babelify = require("babelify")
const source = require('vinyl-source-stream')

task('js', () => {
  return browserify({
      entries: ['./src/index.jsx'],
      debug: true,
      transform: [
        babelify.configure({ 
          presets: ['@babel/preset-env'],
          plugins: [
            ["@babel/plugin-transform-react-jsx", {"pragma": "xml"}],
          ],
        })
      ],
      insertGlobals: true,
    })
    .bundle()
    .pipe(source('index.js'))
    .pipe(dest('dist'))
})

task('htm', () => {
  return src('./src/*.htm')
    .pipe(dest('./dist'))
})

task('img', () => {
  return src('./src/*.ico')
    .pipe(dest('./dist'))
})

exports.default = parallel('js', 'htm', 'img')
