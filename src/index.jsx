const { client, xml, jid } = require("@xmpp/client")
const { HoolClient } = require("hool-client")
import "core-js/stable"
import "regenerator-runtime/runtime"

module.exports = {
  HoolClient
}

const hool = new HoolClient({
	service: "wss://hool.org:443/xmpp-websocket",
	resource: "hoolfront",
})

// set up all the data
let state = {}
state.hands = new Map()
state.info = new Map()
state.currentCard = {}

// chats are data too!
state.chats = {}

// some helper functions
function prettySuit(suit) {
	switch (suit) {
		case 'C': {
			return '♣'
			break
		}
		case 'D': {
			return '♦'
			break
		}
		case 'H': {
			return '♥'
			break
		}
		case 'S': {
			return '♠'
			break
		}
		default: {
			return suit
		}
	}
}

function suitColour(suit) {
	// calculate fun colour
	switch (suit) {
		case 'C': {
			return'primary'
			break
		}
		case 'D': {
			return 'warning'
			break
		}
		case 'H': {
			return'error'
			break
		}
		case 'S': {
			return 'dark'
			break
		}
		case 'HCP':
		case 'pattern': {
			return'success'
			break
		}
		default: {
			return 'default'
		}
	}
}

function handleBid (bid) {
	if(!!bid.type) {
			let wonString = bid.won ? ' and won' : ''
			if (bid.type == 'level') {
				console.info(`${bid.side} has bid: ${bid.level}${bid.suit}${wonString}`)
			} else {
				console.info(`${bid.side} has bid: ${bid.type}${wonString}`)
			}

			// display bid on screen
			let bidSpan = document.querySelector(`#otherbids span[data-side=${bid.side}]`)

			// create if not exist
			if (!bidSpan) {
				bidSpan = document.createElement('span')
				bidSpan.classList.add('chip')
				bidSpan.dataset.side = bid.side

				let bidIcon = document.createElement('figure')
				bidIcon.classList.add('avatar')
				bidIcon.classList.add('avatar-sm')
				bidSpan.appendChild(bidIcon)

				let bidSpanSpan = document.createElement('span')
				bidSpan.classList.add('bid')
				bidSpan.appendChild(bidSpanSpan)

				document.getElementById('otherbids').appendChild(bidSpan)
			}

			// reset class list (we'll update it later)
			bidSpan.querySelector('span').classList = []

			// update value
			if (bid.type == 'level') {

				// calculate pretty suit
				bid.prettySuit = prettySuit(bid.suit)
				bidSpan.querySelector('span').innerHTML = `${bid.level}${bid.prettySuit}`

				// set the colour too
				bidSpan.querySelector('span').classList = [`text-${suitColour(bid.suit)}`]

				// update 'won' status if necessary
				if (bid.won) {
					document.querySelectorAll('#otherbids .avatar.bg-success').forEach(e => {
						e.classList.remove('bg-success')
					})

					bidSpan.querySelector('.avatar').classList.add('bg-success')
				}
			} else if (bid.type == 'redouble') {
				bidSpan.querySelector('span').innerHTML += ' XX'
			} else if (bid.type == 'double') {
				bidSpan.querySelector('span').innerHTML = 'X'

				// highlight the double
				if (bid.won) {
					bidSpan.querySelector('figure').classList.add('bg-warning')
				}
			} else if (bid.type == 'pass') {
				if (Array.from(bidSpan.querySelector('.avatar').classList).includes('bg-success')) {
					bidSpan.querySelector('span').innerHTML += ' P'
				} else {
					bidSpan.querySelector('span').innerHTML = 'P'
				}
			} else {
				bidSpan.querySelector('span').innerHTML = bid.type
			}
	} else {
		console.info(`${bid.side} has made a bid`)
	}
}

hool.on('connected', (e) => {
	alert('We are now connected!')
	document.getElementById("online").style.display = "block"
	document.getElementById("offline").style.display = "none"

	document.getElementById('btn-connect').classList.add('d-hide')
	document.getElementById('btn-disconnect').classList.remove('d-hide')

	// focus the table service selector
	document.getElementById('table_service').focus()
})

hool.on('tablePresence', (e) => {
	let role = !e.role || e.role == 'none' ? 'kibitzer' : e.role
	let message = `${e.user} has joined ${e.table} as ${role} (${e.affiliation || 'member'})`

	// we're doing this because too many alerts() messes the flow
	console.info(message)

	// if it's us, update side.
	if(hool.jid.bare().equals(jid(e.user).bare())) {
		state.side = role
	}
})

hool.on('tableJoinError', (e) => {
	alert(`Failed to join table: ${e.tag || 'cancel'} (${e.type || 'unknown-error'}): ${e.text || 'unknown error'}`)
})

hool.on('tableLeave', (e) => {
	console.info(`${e.user} has (temporarily) disconnected from ${e.table}`)
})

hool.on('tableExit', (e) => {
	console.info(`${e.user} has (permanently) left ${e.table}`)
})

hool.on('invited', (e) => {
	console.info(`We (${e.invitee}) have been invited by ${e.invitor} to play ${e.role} on ${e.table}`)

	if (confirm(`${e.invitor} has invited us to play ${e.role} on ${e.table}! Would you like to join?`)) {
		// Leave table if we're on one
		if (document.getElementById('table_id').value) {
			leaveTable()
		}

		document.getElementById('table_id').value = e.table
	  document.getElementById('table_role').value = e.role

		hool.joinTable(e.table, e.role)
	} else {
		hool.decline(e.table, e.invitor, e.role)
	}
})

hool.on('invite-declined', (e) => {
	console.info(`${e.invitee} declined our invitation to play ${e.role} on ${e.table}`)
})

hool.on('roster-subscribe', (e) => {
	console.log(`Subscription request from ${e.user}`)
	if (confirm(`${e.user} would like to add you as a friend. Do you accept?`)) {
	  hool.rosterSubscribed(e.user)
	  hool.rosterSubscribe(e.user)
	} else {
		console.log(`${e.user} is not your friend`)
	}
})

hool.on('roster-subscribed', (e) => {
	console.log(`${e.user} has accepted your friend request!`)
	alert(`${e.user} has accepted your friend request`)

	// add to friend list
	friendToRoster(jid(e.user).bare().toString())
})

hool.on('roster-unsubscribe', async(e) => {
	console.log(`${e.user} is no longer following your presence`)
    await hool.rosterRemove(e.user)
})

hool.on('roster-unsubscribed', (e) => {
	console.log(`${e.user} has removed you as their friend`)

})

hool.on('contact-online', (e) => {
	console.log(`${e.user} is now online (${e.show || 'unknown'}): ${e.status || 'no status message'}`)
})

hool.on('contact-offline', (e) => {
	console.log(`${e.user} is now offline`)
})

hool.on('chat', (e) => {
	console.info(`${e.from} says: ${e.text}`)

	// get bare JID
	let bareFrom = jid(e.from).bare().toString()

	// add it to the log
	if (!state.chats[bareFrom]) {
		state.chats[bareFrom] = []
	}

	state.chats[bareFrom].push({
		from: e.from,
		text: e.text,
	})

	// add the message
	renderChat('chat-text', e.from, e.text)

	// switch to that user if needed
	switchChatUser(bareFrom)
})

hool.on('groupchat', (e) => {
	console.info(`${e.from} says: ${e.text}`)

  // add the message
	renderChat('group-chat-text', e.from, e.text)
})

hool.on('stateUpdate', (e) => {
	console.log('state update!')
	console.log(e)

	// display hands
	if (state.side && state.side != 'none' && e.hands) {
		for (let h of e.hands) {
			state.hands.set(h.side, h.cards)
		}

		e.hands.forEach((h) => {
			// get or create the appropriate div
			let sideDiv = document.querySelector(`.cards .hand[data-side="${h.side}"]`)
			if (!sideDiv) {
				sideDiv = document.createElement('div')
				sideDiv.classList.add('hand')
				sideDiv.dataset.side = h.side

				document.querySelector('.cards').appendChild(sideDiv)
			}

			displayCards(sideDiv, h)
		})
	}

	// display bids
	if (!!e.bids) {
		e.bids.forEach(handleBid)
	}

	// display score
	if (e.score) {
		let scoreString
		if (e.score.trick) {
			scoreString = e.score.trick.map(s => `${s.side}-${s.value}`).join(' ')
			console.info(`Trick score: ${scoreString}`)
		}
		if (e.score.deal) {
			scoreString = e.score.deal.map(s => `${s.side}-${s.value}`).join(' ')
			console.info(`Deal score: ${scoreString}`)
		}
		if (e.score.running) {
			scoreString = e.score.running.map(s => `${s.side}-${s.value}`).join(' ')
			console.info(`Running score: ${scoreString}`)
		}
	}

	// handle host change
	if (e.tableinfo && e.tableinfo.host) {
		console.info(`${e.tableinfo.host} is the new host for ${e.table}`)
	}
})

hool.on('infoshare', (e) => {
	console.info(`${e.side} has shared ${e.type} info: ${e.value}`)

	// re-sort pattern
	if (e.type == 'pattern') {
		let pat = e.value.split(',').sort().reverse()

		// make sure it has four values
		while (pat.length < 4) {
			pat.push('0')
		}

		// put them together
		e.value = pat.join(',')
	}

	let s = state.info.get(e.side)
	if (!s) s = []
	s.push({type: e.type, value: e.value})
	state.info.set(e.side, s)

	let otherShares = document.getElementById('othershares')
	let sideShare = document.getElementById(`othershare-${e.side}`)
	if (!sideShare) {
		sideShare = document.createElement('p')
		sideShare.innerHTML = '<b>' + e.side + ' shared: </b>`'
		sideShare.id = `othershare-${e.side}`
		otherShares.appendChild(sideShare)
	}

	let i = document.createElement('span')
	i.classList.add('chip')

	// badge
	let b = document.createElement('figure')
	b.classList.add('avatar')
	b.classList.add('avatar-sm')

	// decide pretty type for badge
	switch (e.type) {
		case 'C': {
			b.dataset.initial = '♣'
			b.classList.add('bg-primary')
			break
		}
		case 'D': {
			b.dataset.initial = '♦'
			b.classList.add('bg-warning')
			break
		}
		case 'H': {
			b.dataset.initial = '♥'
			b.classList.add('bg-error')
			break
		}
		case 'S': {
			b.dataset.initial = '♠'
			b.classList.add('bg-dark')
			break
		}
		case 'HCP': {
			b.dataset.initial = '★'
			b.classList.add('bg-success')
			break
		}
		case 'pattern': {
			b.dataset.initial = '⚖'
			b.classList.add('bg-success')
			break
		}
		default: {
			b.dataset.initial = e.type
		}
	}

	i.appendChild(b)
	i.innerHTML += e.value

	sideShare.appendChild(i)
})

hool.on('infoshareError', (e) => {
	alert(`Failed to share info to ${e.table || 'the table'}: ${e.tag || 'unknown error'}: ${e.text || 'unknown error'}`)
})

hool.on('cardplay', (e) => {
	// hide the card, if necessary

	(document.querySelectorAll(`.cards button[data-rank="${e.rank}"][data-suit="${e.suit}"]`)
		.forEach(c => {
			c.classList.add('d-hide')
		}))

	console.info(`${e.side} has played card ${e.rank}${e.suit}`)

	// calculate pretty suit
	switch (e.suit) {
		case 'C': {
			e.prettySuit = '♣'
			break
		}
		case 'D': {
			e.prettySuit = '♦'
			break
		}
		case 'H': {
			e.prettySuit = '♥'
			break
		}
		case 'S': {
			e.prettySuit = '♠'
			break
		}
		default: {
			e.prettySuit = e.suit
		}
	}

	// display cards on screen
	let cardDiv = document.getElementById('playedcards')

	// clear display if already 4
	if (cardDiv.childElementCount >= 4) {
		cardDiv.innerHTML = ''
	}

	let cardBadge = document.createElement('span')
	cardBadge.classList.add('chip')

	let cardBadgeIcon = document.createElement('figure')
	cardBadgeIcon.classList.add('avatar')
	cardBadgeIcon.classList.add('avatar-sm')
	cardBadgeIcon.dataset.initial = e.side
	cardBadge.appendChild(cardBadgeIcon)

	cardBadge.innerHTML += `${e.prettySuit}${e.rank}`
	cardBadge.classList.add(`text-${suitColour(e.suit)}`)

	cardDiv.appendChild(cardBadge)
})

hool.on('cardplayError', (e) => {
	let line = `Failed to play ${e.rank}${e.suit}: ${e.tag || 'unknown error'}: ${e.text || 'unknown error'}`
	console.warn(line)
	alert(line)
})


hool.on('bid', handleBid)

hool.on('bidError', (e) => {
	alert(`Failed to make bid on ${e.table || 'the table'}: ${e.tag || 'unknown error'}: ${e.text || 'unknown error'}`)
})

hool.on('undo', (e) => {
	console.log("Undo",e);
})

hool.on('undoError', (e) => {
	alert(`Failed to make undo on ${e.table || 'the table'}: ${e.tag || 'unknown error'}: ${e.text || 'unknown error'}`)
})

hool.on('claim', (e) => {
	console.log("claim",e);
})

hool.on('claimError', (e) => {
	alert(`Failed to make claim on ${e.table || 'the table'}: ${e.tag || 'unknown error'}: ${e.text || 'unknown error'}`)
})

hool.on('scoreboard', (e) => {
	console.log("scoreboard",e);
})

hool.on('scoreboardError', (e) => {
	alert(`Failed to make scoreboard request on ${e.table || 'the table'}: ${e.tag || 'unknown error'}: ${e.text || 'unknown error'}`)
})

hool.on('offline', (e) => {
	document.getElementById("online").style.display = "none";document.getElementById("offline").style.display = "block";
	document.getElementById('btn-connect').style.display = "inline-block"
	document.getElementById('btn-connect').classList.remove('loading')
	document.getElementById('btn-connect').classList.remove('disabled')
	document.getElementById('btn-connect').classList.remove('d-hide')
	document.getElementById('btn-disconnect').classList.add('d-hide')
})

hool.on('error', (e) => {
	document.getElementById("online").style.display = "none";document.getElementById("offline").style.display = "block";
	document.getElementById('btn-connect').classList.remove('d-hide')
	document.getElementById('btn-connect').classList.remove('loading')
	document.getElementById('btn-connect').classList.add('disabled')
	document.getElementById('btn-disconnect').classList.remove('d-hide')

	console.log('error:', e)
	alert(`Error: ${e.code}: ${e.message}`)

	hool.xmppStop()
	document.getElementById('btn-disconnect').classList.add('d-hide')
	document.getElementById('btn-connect').classList.remove('disabled')
})

function displayCards(cardDiv, hand) {
	cardDiv.style.removeProperty('display')
	cardDiv.innerHTML = `<h4>${hand.side ? hand.side + "'s" : "Your"} cards</h4>`
	for (let card of hand.cards) {
		let c = document.createElement('button')
		c.classList.add('btn')
		c.classList.add('text-large')

		// Save the data
		c.dataset.rank = card.rank
		c.dataset.suit = card.suit
		c.dataset.side = hand.side

		c.onclick = function (event) {
			event.preventDefault()
			let card = event.target.dataset

			// calculate details
			let tableID = document.getElementById('table_id').value
			state.currentCard.side =  hand.side || state.side
			state.currentCard.rank = card.rank
			state.currentCard.suit = card.suit
			// send the message
			hool.playCard(tableID, hand.side || state.side, card.rank, card.suit)
		}

		// Prettify suit
		switch (card.suit) {
			case 'C': {
				card.prettySuit = '♣'
				c.classList.add('bg-primary')
				break
			}
			case 'D': {
				card.prettySuit = '♦'
				c.classList.add('bg-warning')
				break
			}
			case 'H': {
				card.prettySuit = '♥'
				c.classList.add('bg-error')
				break
			}
			case 'S': {
				card.prettySuit = '♠'
				c.classList.add('bg-dark')
				break
			}
			default: {
				c.prettySuit = c.suit
			}
		}
		c.innerText = `${card.prettySuit}${card.rank}`

		cardDiv.appendChild(c)
	}
}

function xmppConnect() {

	// read credentials from page
	let myJID = document.getElementById('username').value
	let myPassword = document.getElementById('password').value

  document.getElementById('btn-connect').classList.add('loading')

	hool.xmppConnect(myJID, myPassword)
}

function xmppDisconnect() {
	hool.xmppStop()
}

async function xmppSetService() {
	hool.tableService = document.getElementById('table_service').value
	alert('Using table service: ' + hool.tableService)

	// focus the table selector
	document.getElementById('table_id').focus()
}

async function xmppDiscoverServices() {
	alert ('Connected to table service: ' + (await hool.xmppDiscoverServices()))
	document.getElementById('table_service').placeholder = hool.tableService
}

async function xmppListTables() {
	alert ('Found tables: ' + (await hool.getTableList()).map((r)=>{return r.jid}))
}

async function joinTable() {
	let tableID = document.getElementById('table_id').value
	let role = document.getElementById('table_role').value

	console.log(`Joining table ${tableID} as ${role}`)
	alert(`Joining table ${tableID} as ${role}`)
	hool.joinTable(tableID, role)
}

function leaveTable() {
	let tableID = document.getElementById('table_id').value

	console.log(`Leaving table ${tableID}`)
	hool.leaveTable(tableID)
}

function invite() {
	let tableID = document.getElementById('table_id').value
	let userID = document.getElementById('invite-user').value
	let inviteRole = document.getElementById('invite-role').value

	console.log(`Inviting ${userID} as ${inviteRole} on ${tableID}`)

	hool.invite(tableID, userID, inviteRole, 'You have been invited to a game!')
}

function shareInfo() {
	let tableID = document.getElementById('table_id').value
	let infoType = document.getElementById('info_type').value

	// TODO: autocalculate info value and share that as well

	hool.shareInfo(tableID, state.side, infoType)
}

function undoRequest() {
	let tableID = document.getElementById('table_id').value

	console.info(`${state.side} undo Request`)
	hool.undo(tableID, 'request' , {side:state.currentCard.side, rank:state.currentCard.rank,  suit:state.currentCard.suit})
}

function undoResponse() {
	let tableID = document.getElementById('table_id').value
	let undo_status = document.getElementById('undo_status').value
	// TODO: autocalculate info value and share that as well
	console.info(`${state.side} undo Response`)
	hool.undo(tableID , undo_status, {})
}

function undoCancel() {
	let tableID = document.getElementById('table_id').value
		// TODO: autocalculate info value and share that as well
		console.info(`${state.side} undo Cancel`)
	//hool.undoCancel(tableID , state.side)
	hool.undo(tableID , 'cancel', {side: state.side})
}

function changeRequest(){
	let tableID = document.getElementById('table_id').value
	console.info(`${state.side} change Request`)
	change_type = document.getElementById('change_type').value
	hool.changedeal(tableID , state.side, change_type)
}

function makeBid() {
	let tableID = document.getElementById('table_id').value
	let bidType = document.getElementById('bid_type').value
	let bidLevel = document.getElementById('bid_level').value
	let bidSuit = document.getElementById('bid_suit').value

	// TODO: autocalculate info value and share that as well

	hool.makeBid(tableID, state.side, bidType, bidLevel, bidSuit)
}

function claimRequest(){
	let tableID = document.getElementById('table_id').value
	let claimAmount = document.getElementById('claim_amount').value

	// TODO: autocalculate info value and share that as well

	hool.claim(tableID, 'request', {amount:claimAmount, side: state.side})
}

function claimResponse() {
	let tableID = document.getElementById('table_id').value
	let claimAmount = document.getElementById('claim_amount').value
	let claimType = document.getElementById('claim_type').value
	// TODO: autocalculate info value and share that as well
	if(claimType == 'withdraw')
		hool.claim(tableID, claimType, {side: state.side})
	else
		hool.claim(tableID, claimType, {})
}

function scoreboardResponse(resp) {
	let tableID = document.getElementById('table_id').value
	hool.scoreboard(tableID, resp==1?'ready':'notready', state.side)
}

function catchupTable(resp) {
	let tableID = document.getElementById('table_id').value
	hool.catchup(tableID, 'catchup', state.side)
}

function addBot(resp){
	let tableID = document.getElementById('bot_table_name').value
	let side =  document.getElementById('bot_side').value
	hool.addBot(tableID, side)
}

function renderChat(elementId, sender, message) {
	let elTile = document.createElement('div')
	elTile.classList.add('tile')

	let elImage = document.createElement('div')
	elImage.classList.add('tile-icon')
	elTile.appendChild(elImage)

	let elAvatar = document.createElement('figure')
	elAvatar.classList.add('avatar')
	elImage.appendChild(elAvatar)

	let elContent = document.createElement('div')
	elContent.classList.add('tile-content')
	elTile.appendChild(elContent)

	let elName = document.createElement('p')
	elName.classList.add('tile-title')
	elName.classList.add('text-bold')
	elName.classList.add('text-break')
	elName.textContent = sender
	elContent.appendChild(elName)

	let elMessage = document.createElement('p')
	elName.classList.add('tile-subtitle')
	
	elMessage.textContent = message
	elContent.appendChild(elMessage)

  let mainEl = document.getElementById(elementId || 'group-chat-text')
  mainEl.appendChild(elTile)

  // automatically scroll to the bottom
  mainEl.scrollTop = mainEl.scrollHeight
}

function switchChatUser(user) {
	// clear the chatbox
  let chatBox = document.getElementById('chat-text')
  chatBox.innerText = ''

  // update if necessary
  if (state.chats[user]) {
		for (let chat of state.chats[user]) {
			renderChat('chat-text', chat.from, chat.text)
		}
	}

	// adjust the select
	let friendSelect = document.getElementById('select-friend')
	if (friendSelect.value != user) {
		friendToRoster(user)
		document.getElementById('select-friend').value = user
	}
}

function updateChat() {
	let user = document.getElementById('select-friend').value

	switchChatUser(user)
}

function groupChat() {
	let tableID = document.getElementById('table_id').value
	let messageText = document.getElementById('group-chat-input').value

	hool.groupChat(tableID, messageText)
	document.getElementById('group-chat-input').value = ''

	// add the message
	renderChat('group-chat-text', 'me', messageText)
}

function chat() {
	let user = document.getElementById('select-friend').value
	let messageText = document.getElementById('friend-chat-input').value

	hool.chat(user, messageText)

	// add the message
	renderChat('chat-text', 'me', messageText)

	// add it to the log
	if (!state.chats[user]) {
		state.chats[user] = []
	}

	state.chats[user].push({
		from: 'me',
		text: messageText,
	})

	// clear the value
	document.getElementById('friend-chat-input').value = ''
}

function rosterAdd() {
	let userID = document.getElementById('invite-user').value
	hool.rosterSubscribed(userID) // pre-emptively grant subscription request
	hool.rosterSubscribe(userID)
}

function rosterReject() {
	let userID = document.getElementById('invite-user').value

	hool.rosterUnsubscribed(userID)
}

async function rosterRemove() {
	let userID = document.getElementById('invite-user').value
	console.log("roster removing")
	hool.rosterUnsubscribed(userID) // stop them following us
	await hool.rosterRemove(userID) // don't follow them
}

function friendToRoster(userJID, name=undefined) {
	if (!document.getElementById(`option-friend-${userJID}`)) {
	  let elOption = document.createElement('option')
	  elOption.id = `option-friend-${userJID}`
	  elOption.value = userJID
	  elOption.innerText = name || userJID

	  document.getElementById('select-friend').appendChild(elOption)
	}
}

async function xmppGetRoster() {
	let roster = await hool.xmppGetRoster()
	console.log(`Found friends: `, roster.map((r)=>{return r.jid}))

	if (roster.length) {
		alert('Found friends:\n' + roster.map((r) => `${r.name} (${r.jid})`))
	} else {
		alert("You don't have any friends! :(")
	}

	state.roster = roster

	// add them to the select!
	for (let contact of roster) {
		friendToRoster(contact.jid, contact.name)
	}
}

// register listeners!

// switch to password field on username enter
document.getElementById('username').addEventListener('keypress', e => {
	if (e.key == 'Enter')  {
		e.preventDefault()

		if (!!document.getElementById('username').value) {
			document.getElementById('password').focus()
		}
	}
})

document.getElementById('btn-connect').onclick = xmppConnect

// connect on enter, if not already connected
document.getElementById('password').addEventListener('keypress', e => {
	if (e.key == 'Enter')  {
		e.preventDefault()

		if (
		  !!document.getElementById('username').value &&
		  !!document.getElementById('password').value &&
		  !document.getElementById('btn-connect').classList.contains('d-hide') &&
		  !document.getElementById('btn-connect').classList.contains('loading') &&
		  !document.getElementById('btn-connect').classList.contains('disabled')
		) {
			xmppConnect()
		}
	}
})

document.getElementById('btn-disconnect').onclick = xmppDisconnect
document.getElementById('btn-set-service').onclick = xmppSetService

// automatically set the table service on enter
document.getElementById('table_service').addEventListener('keypress', e => {
	if (e.key == 'Enter')  {
		e.preventDefault()

		if (!!document.getElementById('table_service').value) {
			xmppSetService()
		}
	}
})

document.getElementById('btn-discover-services').onclick = xmppDiscoverServices
document.getElementById('btn-list-tables').onclick = xmppListTables
document.getElementById("btn-join-table").onclick = joinTable
document.getElementById("btn-leave-table").onclick = leaveTable
document.getElementById("btn-catchup").onclick = catchupTable
document.getElementById('btn-share-info').onclick = shareInfo
document.getElementById('btn-make-bid').onclick = makeBid
document.getElementById('btn-undo').onclick = undoRequest
document.getElementById('btn-undo-response').onclick = undoResponse
document.getElementById('btn-undo-cancel').onclick = undoCancel
document.getElementById('btn-change-deal').onclick = changeRequest
document.getElementById('btn-claim').onclick = claimRequest
document.getElementById('btn-claim-response').onclick = claimResponse
document.getElementById('btn-play').onclick = () => { scoreboardResponse(1) }
document.getElementById('btn-pause').onclick = () => { scoreboardResponse(1) }
document.getElementById('btn-add-bot').onclick = addBot
document.getElementById('btn-invite').onclick = invite
document.getElementById('btn-group-chat').onclick = groupChat

// send groupchat message on enter
document.getElementById('group-chat-input').addEventListener('keypress', e => {
	if (e.key == 'Enter')  {
		e.preventDefault()

		if (!!document.getElementById('group-chat-input').value) {
			groupChat()
		}
	}
})

document.getElementById('btn-roster-add').onclick = rosterAdd
document.getElementById('btn-roster-reject').onclick = rosterReject
document.getElementById('btn-roster-remove').onclick = rosterRemove
document.getElementById('btn-list-friends').onclick = xmppGetRoster

document.getElementById('select-friend').onchange = updateChat

document.getElementById('btn-send-chat').onclick = chat

// send chat message on enter
document.getElementById('friend-chat-input').addEventListener('keypress', e => {
	if (e.key == 'Enter')  {
		e.preventDefault()

		if (!!document.getElementById('friend-chat-input').value) {
			chat()
		}
	}
})
